<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizCategory extends Model
{
    protected $table = "quiz_categories";

    public function creator()
    {
    	return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function updater()
    {
    	return $this->belongsTo('App\User', 'updated_by', 'id');
    }
}
