<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;
use Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = true;
        $errors = array();
        $query = User::where('deleted_at',null);
        if($request->has('order')){
            $arrorder = explode(',',$request->get('order'));
            foreach ($arrorder as $value) {
                if(substr($value,0,1) == '-'){
                    $query->orderBy(str_replace('-','',$value),'desc');
                }else{
                    $query->orderBy($value,'asc');
                }
            }
        }
        $data['users'] = $query->paginate(10);
        return response()->json(compact('status','errors','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $status = true;
        $errors = array();
        $validation = Validator::make($request->all(),[ 
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password'
        ]);
        if($validation->fails()){
            $status = false;
            $errors = $validation->errors();
        }else{
            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->save();

            $data['user'] = $user;
        }
        return response()->json(compact('status','errors','data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = true;
        $errors = array();
        $data['user'] = User::find($id);
        return response()->json(compact('status','errors','data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $status = true;
        $errors = array();
        $validation = Validator::make($request->all(),[ 
            'name' => 'required'
        ]);
        if($validation->fails()){
            $status = false;
            $errors = $validation->errors();
        }else{
            $user = User::find($id);
            $user->name = $request->name;
            $user->save();

            $data['user'] = $user;
        }
        return response()->json(compact('status','errors','data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = true;
        $errors = array();
        User::find($id)->delete();
        return response()->json(compact('status','errors','data'));
    }
}
