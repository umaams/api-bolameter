<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\QuizCategory;
use App\Quiz;
use Auth;
use Validator;

class QuizCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = true;
        $errors = array();
        $arrincludes = ($request->has('includes')) ? explode(',', $request->get('includes')) : array(); 
        
        $query = QuizCategory::where('deleted_at',null);
        if (in_array('creator', $arrincludes)) {
            $query->with(['creator' => function ($query) {
                $query->select('id', 'name');
            }]);
        }
        if (in_array('updater', $arrincludes)) {
            $query->with(['updater' => function ($query) {
                $query->select('id', 'name');
            }]);
        }
        if ($request->has('order')) {
            $arrorder = explode(',',$request->get('order'));
            foreach ($arrorder as $value) {
                if (substr($value,0,1) == '-') {
                    $query->orderBy(str_replace('-','',$value),'desc');
                } else {
                    $query->orderBy($value,'asc');
                }
            }
        }
        $data['quiz_categories'] = $query->paginate($request->has('per_page') ? $request->get('per_page') : 10);
        return response()->json(compact('status','errors','data'));
    }

    public function showAll(Request $request)
    {
        $status = true;
        $errors = array();
        $query = QuizCategory::with(['creator','updater'])->where('deleted_at',null);
        if($request->has('order')){
            $arrorder = explode(',',$request->get('order'));
            foreach ($arrorder as $value) {
                if(substr($value,0,1) == '-'){
                    $query->orderBy(str_replace('-','',$value),'desc');
                }else{
                    $query->orderBy($value,'asc');
                }
            }
        }
        $data['quiz_categories'] = $query->get();
        return response()->json(compact('status','errors','data'));
    }

    public function quiz($id, Request $request)
    {
        $status = true;
        $errors = array();
        $arrincludes = ($request->has('includes')) ? explode(',', $request->get('includes')) : array(); 
        
        $query = Quiz::where('deleted_at',null)->where('quiz_category_id', $id);
        if (in_array('creator', $arrincludes)) {
            $query->with(['creator' => function ($query) {
                $query->select('id', 'name');
            }]);
        }
        if (in_array('updater', $arrincludes)) {
            $query->with(['updater' => function ($query) {
                $query->select('id', 'name');
            }]);
        }
        if ($request->has('order')) {
            $arrorder = explode(',',$request->get('order'));
            foreach ($arrorder as $value) {
                if (substr($value,0,1) == '-') {
                    $query->orderBy(str_replace('-','',$value),'desc');
                } else {
                    $query->orderBy($value,'asc');
                }
            }
        }
        $data['quizes'] = $query->paginate($request->has('per_page') ? $request->get('per_page') : 10);
        return response()->json(compact('status','errors','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $status = true;
        $errors = array();
        $validation = Validator::make($request->all(),[ 
            'name' => 'required'
        ]);
        if($validation->fails()){
            $status = false;
            $errors = $validation->errors();
        }else{
            $quiz_category = new QuizCategory;
            $quiz_category->name = $request->name;
            $quiz_category->created_by = Auth::user()->id;
            $quiz_category->updated_by = Auth::user()->id;
            $quiz_category->save();

            $data['quiz_category'] = $quiz_category;
        }
        return response()->json(compact('status','errors','data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = true;
        $errors = array();
        $data['quiz_category'] = QuizCategory::find($id);
        return response()->json(compact('status','errors','data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $status = true;
        $errors = array();
        $validation = Validator::make($request->all(),[ 
            'name' => 'required'
        ]);
        if($validation->fails()){
            $status = false;
            $errors = $validation->errors();
        }else{
            $quiz_category = QuizCategory::find($id);
            $quiz_category->name = $request->name;
            $quiz_category->description = $request->description;
            $quiz_category->updated_by = Auth::user()->id;
            $quiz_category->save();

            $data['quiz_category'] = $quiz_category;
        }
        return response()->json(compact('status','errors','data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = true;
        $errors = array();
        QuizCategory::find($id)->delete();
        return response()->json(compact('status','errors','data'));
    }
}
