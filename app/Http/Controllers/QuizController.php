<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;
use Auth;
use Validator;

class QuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = true;
        $errors = array();
        $query = Quiz::where('deleted_at',null);
        if ($request->has('with')) {
            $arrwith = explode(',', $request->get('with'));
            foreach ($arrwith as $item) {
                switch ($item) {
                    case 'creator':
                        $query->with(['creator' => function ($query) {
                            $query->select('id', 'name');
                        }]);
                        break;
                    case 'updater':
                        $query->with(['updater' => function ($query) {
                            $query->select('id', 'name');
                        }]);
                        break;
                    case 'quiz_category':
                        $query->with(['quiz_category' => function ($query) {
                            $query->select('id', 'name');
                        }]);
                        break;
                    default:
                        break;
                }
            }
        }
        if ($request->has('order')) {
            $arrorder = explode(',',$request->get('order'));
            foreach ($arrorder as $value) {
                if(substr($value,0,1) == '-'){
                    $query->orderBy(str_replace('-','',$value),'desc');
                }else{
                    $query->orderBy($value,'asc');
                }
            }
        }
        $data = $query->paginate($request->has('per_page') ? intval($request->get('per_page')) : 10);
        return response()->json(compact('status','errors','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $status = true;
        $errors = array();
        $validation = Validator::make($request->all(),[ 
            'name' => 'required',
            'quiz_category_id' => 'required'
        ]);
        if($validation->fails()){
            $status = false;
            $errors = $validation->errors();
        }else{
            $quiz = new Quiz;
            $quiz->name = $request->name;
            $quiz->quiz_category_id = $request->quiz_category_id;
            $quiz->description = $request->description;
            $quiz->created_by = Auth::user()->id;
            $quiz->updated_by = Auth::user()->id;
            $quiz->save();

            $data['quiz'] = $quiz;
        }
        return response()->json(compact('status','errors','data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $status = true;
        $errors = array();
        $arrincludes = ($request->has('includes')) ? explode(',', $request->get('includes')) : array(); 
        
        $query = Quiz::where('deleted_at', null);
        if (in_array('creator', $arrincludes)) {
            $query->with(['creator' => function ($query) {
                $query->select('id', 'name');
            }]);
        }
        if (in_array('updater', $arrincludes)) {
            $query->with(['updater' => function ($query) {
                $query->select('id', 'name');
            }]);
        }
        if (in_array('random_questions', $arrincludes)) {
            $query->with(['questions' => function ($query) use ($id, $request, $arrincludes) {
                if (in_array('answers', $arrincludes)) {
                    $query->with('answers');
                }
                return $query->inRandomOrder()->take(Quiz::find($id)->question_per_play);
            }]);
        }
        $data['quiz'] = $query->find($id);
        return response()->json(compact('status','errors','data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $status = true;
        $errors = array();
        $validation = Validator::make($request->all(),[ 
            'name' => 'required',
            'quiz_category_id' => 'required'
        ]);
        if($validation->fails()){
            $status = false;
            $errors = $validation->errors();
        }else{
            $quiz = Quiz::find($id);
            $quiz->name = $request->name;
            $quiz->quiz_category_id = $request->quiz_category_id;
            $quiz->description = $request->description;
            $quiz->updated_by = Auth::user()->id;
            $quiz->save();

            $data['quiz'] = $quiz;
        }
        return response()->json(compact('status','errors','data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = true;
        $errors = array();
        Quiz::find($id)->delete();
        return response()->json(compact('status','errors','data'));
    }
}
