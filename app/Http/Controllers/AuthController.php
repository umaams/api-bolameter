<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Validator;
use Auth;

class AuthController extends Controller
{

    public function login(Request $request)
    {
        $validation = Validator::make($request->all(),[ 
            'email' => 'required',
            'password' => 'required'
        ]);
        if(!$validation->fails()){
            $credentials = $request->only('email', 'password');
            try {
                if (!$token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            } catch (JWTException $e) {
                return response()->json(['error' => 'could_not_create_token'], 500);
            }
            return response()->json(
                [
                    'status'    => true,
                    'error'     => '',
                    'data'      => [ 'access_token' => $token ]
                ]
            );
        }else{
            return response()->json([
                'status' => false,
                'error' => 'Email and Password required'
            ], 200);
        }
    }

    public function me()
    {
        $status = true;
        $error = '';
        $data['user'] = Auth::user();
        return response()->json(compact('status', 'error', 'data'));
    }

    public function logout()
    {
        $this->guard()->logout();

        return response()->json([
            'status' => true,
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }
}