<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    protected $table = "quizes";

    public function quiz_category()
    {
    	return $this->belongsTo('App\QuizCategory','quiz_category_id','id');
    }

    public function creator()
    {
    	return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function updater()
    {
    	return $this->belongsTo('App\User', 'updated_by', 'id');
    }

    public function questions()
    {
        return $this->hasMany('App\Question', 'quiz_id', 'id');
    }
}
