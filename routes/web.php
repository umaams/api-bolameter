<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::group(['prefix' => 'auth'], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::group(['middleware' => 'jwt.refresh'], function ($router) {
    	Route::post('refresh', 'AuthController@refresh');
    });
    Route::group(['middleware' => 'jwt.auth'], function ($router) {
    	Route::get('me', 'AuthController@me');
    	Route::post('logout', 'AuthController@logout');    	
    });
});

Route::get('quiz_category','QuizCategoryController@index')->name('quiz_category.index');
Route::get('quiz_category/{id}/quiz','QuizCategoryController@quiz')->name('quiz_category.quiz');
Route::get('quiz','QuizController@index')->name('quiz.index');
Route::get('quiz/{id}','QuizController@show')->name('quiz.show');

Route::group(['middleware' => 'jwt.auth'], function ($router) {
	Route::get('user','UserController@index')->name('user.index');
	Route::post('user','UserController@store')->name('user.store');
	Route::get('user/{id}','UserController@show')->name('user.show');
	Route::patch('user/{id}','UserController@update')->name('user.update');
	Route::delete('user/{id}','UserController@destroy')->name('user.destroy');

	Route::get('quiz_category/all','QuizCategoryController@showAll')->name('quiz_category.showall');
	Route::post('quiz_category','QuizCategoryController@store')->name('quiz_category.store');
	Route::get('quiz_category/{id}','QuizCategoryController@show')->name('quiz_category.show');
	Route::patch('quiz_category/{id}','QuizCategoryController@update')->name('quiz_category.update');
	Route::delete('quiz_category/{id}','QuizCategoryController@destroy')->name('quiz_category.destroy');

	Route::post('quiz','QuizController@store')->name('quiz.store');
	Route::patch('quiz/{id}','QuizController@update')->name('quiz.update');
	Route::delete('quiz/{id}','QuizController@destroy')->name('quiz.destroy');
});
