<?php

use Illuminate\Database\Seeder;
use App\Quiz;

class QuizesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('quizes')->delete();
        Quiz::Create(['name' => 'Champions of Premier League', 'quiz_category_id' => 1, 'description' => 'Quiz about Champions of Premier League', 'isactive' => '1', 'created_by' => 1, 'updated_by' => 1]);
    }
}
