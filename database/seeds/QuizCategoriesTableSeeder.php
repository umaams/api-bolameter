<?php

use Illuminate\Database\Seeder;
use App\QuizCategory;

class QuizCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('quiz_categories')->delete();
        QuizCategory::Create(['name' => 'Premier League', 'description' => 'Category about Premier League', 'isactive' => '1', 'created_by' => 1, 'updated_by' => 1]);
        QuizCategory::Create(['name' => 'Primera Division', 'description' => 'Category about Primera Division', 'isactive' => '1', 'created_by' => 1, 'updated_by' => 1]);
        QuizCategory::Create(['name' => 'Serie-A Italia', 'description' => 'Category about Serie-A Italia', 'isactive' => '1', 'created_by' => 1, 'updated_by' => 1]);
        QuizCategory::Create(['name' => 'Bundesliga', 'description' => 'Category about Bundesliga', 'isactive' => '1', 'created_by' => 1, 'updated_by' => 1]);
        QuizCategory::Create(['name' => 'League 1', 'description' => 'Category about League 1', 'isactive' => '1', 'created_by' => 1, 'updated_by' => 1]);
        QuizCategory::Create(['name' => 'Indonesian League', 'description' => 'Category about Indonesian League', 'isactive' => '1', 'created_by' => 1, 'updated_by' => 1]);
    }
}
