<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        $user = User::create(['name' => 'Khotibul Umam', 'email' => 'khotib.umam7@gmail.com', 'password' => bcrypt('secret'), 'remember_token' => str_random(10)]);
    }
}
