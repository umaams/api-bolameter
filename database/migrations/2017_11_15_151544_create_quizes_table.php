<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quizes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('quiz_category_id')->unsigned();
            $table->text('description')->nullable();
            $table->enum('type', ['text', 'image'])->default('text');
            $table->enum('isactive', ['0', '1'])->default(0);
            $table->integer('point_per_question')->default(0);
            $table->integer('time_per_question')->default(10);
            $table->integer('question_per_play')->default(10);
            $table->text('image_url')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('quiz_category_id')->references('id')->on('quiz_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quizes');
    }
}
